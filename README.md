# What is it ?
 
Secure-Token is a lightweight library to generate token encrypted using RSA key.
These tokens can be shared to anyone without beeing able to decrypt them without your RSA private key.
 
It's also using a `hmac` to sign the token.
 
# How to import it ?
 
```bash
go get gitlab.com/melvin.biamont/secure-token
```
 
# How to use it ?

## Generate a RSA key-pair

There're a lot of tutorial on Internat about that..

```bash
openssl genrsa -out rsa-private.pem 2048
openssl rsa -in rsa-private.pem -outform PEM -pubout -out rsa-public.pem
```

Don't share your `rsa-private.pem` with anyone !

## Initialize a Tokenizer

```go
tokenizer := secure_token.NewRsaTokenizer()

// There're 3 ways to setup your Tokenizer :

//1. Using the key paths
tokenizer.SetPublicKeyFile(pathPublicKey)
tokenizer.SetPrivateKeyFile(pathPrivateKey)

//2. Using the key string
tokenizer.SetPublicKeyString(publicKey)
tokenizer.SetPrivateKeyString(privateKey)

//3. Using the key bytes
tokenizer.SetPublicKeyBytes(publicKey)
tokenizer.SetPrivateKeyBytes(privateKey)
```

## Generate a token

```go
token := tokenizer.Tokenize(MapClaims{
		"foo": "bar",
})
```

## Parse a token

```go
var c MapClaims

tokenizer.ParseToken(token, &c) //Ensure you pass your claims pointer ;-)
```

