package secure_token

import (
	"encoding/json"
)

type Claims interface {
	ToJson() ([]byte, error)
}

type MapClaims map[string]interface{}

func (m MapClaims) Valid() error {
	//TODO
	return nil
}

func (m MapClaims) ToJson() ([]byte, error) {
	return json.Marshal(m)
}
