package secure_token

import (
	"testing"
	"github.com/pkg/errors"
	"fmt"
)

const pathPrivateKey string = "test/rsa-private.pem"
const pathPublicKey string = "test/rsa-public.pem"

func TestToken(t *testing.T) {
	tokenizer := NewRsaTokenizer()

	err := tokenizer.SetPublicKeyFile(pathPublicKey)

	if err != nil {
		t.Fatal(err)
	}

	err = tokenizer.SetPrivateKeyFile(pathPrivateKey)

	if err != nil {
		t.Fatal(err)
	}

	token, err := tokenizer.Tokenize(MapClaims{
		"foo": "bar",
	})

	if err != nil {
		t.Fatal(err)
	}

	if token == "" {
		t.Fatal(errors.New("Invalid token"))
	}

	fmt.Printf("Token := %s\n", token)

	var c MapClaims

	err = tokenizer.ParseToken(token, &c)

	if err != nil {
		t.Fatal(err)
	}

	if bar, ok := c["foo"]; ok {
		if bar != "bar" {
			t.Fatal(errors.New("Claims data not valid"))
		}
	} else {
		t.Fatal(errors.New("Claims data not existing"))
	}
}
