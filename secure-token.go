package secure_token

import (
	"crypto/rsa"
	"crypto/rand"
	"encoding/base64"
	"crypto/sha256"
	"crypto"
	"bytes"
	"github.com/pkg/errors"
	"encoding/json"
)

const hmac_size int = 10

type RsaTokenizer struct {
	claims     Claims
	publicKey  *rsa.PublicKey
	privateKey *rsa.PrivateKey
}

func NewRsaTokenizer() *RsaTokenizer {
	return &RsaTokenizer{}
}

func (r *RsaTokenizer) SetPublicKeyFile(path string) error {
	pk, err := getRsaPublicKeyFromPath(path)

	if err != nil {
		return err
	}

	r.publicKey = pk
	return nil
}

func (r *RsaTokenizer) SetPublicKeyString(publicKey string) error {
	pk, err := getRsaPublicKeyFromString(publicKey)

	if err != nil {
		return err
	}

	r.publicKey = pk
	return nil
}

func (r *RsaTokenizer) SetPublicKeyBytes(publicKey []byte) error {
	pk, err := getRsaPublicKeyFromBytes(publicKey)

	if err != nil {
		return err
	}

	r.publicKey = pk
	return nil
}

func (r *RsaTokenizer) SetPrivateKeyFile(path string) error {
	pk, err := getRsaPrivateKeyFromPath(path)

	if err != nil {
		return err
	}

	r.privateKey = pk
	return nil
}

func (r *RsaTokenizer) SetPrivateKeyString(publicKey string) error {
	pk, err := getRsaPrivateKeyFromString(publicKey)

	if err != nil {
		return err
	}

	r.privateKey = pk
	return nil
}

func (r *RsaTokenizer) SetPrivateKeyBytes(publicKey []byte) error {
	pk, err := getRsaPrivateKeyFromBytes(publicKey)

	if err != nil {
		return err
	}

	r.privateKey = pk
	return nil
}

func (r *RsaTokenizer) Tokenize(c Claims) (string, error) {
	payload, err := c.ToJson()

	if err != nil {
		return "", err
	}

	data, err := rsa.EncryptPKCS1v15(rand.Reader, r.publicKey, payload)

	if err != nil {
		return "", err
	}

	sha := sha256.Sum256(data)

	sign, err := rsa.SignPKCS1v15(rand.Reader, r.privateKey, crypto.SHA256, sha[:])

	if err != nil {
		return "", err
	}

	data = append(data, sign[0:hmac_size]...)

	return base64.URLEncoding.EncodeToString(data), nil
}

func (r *RsaTokenizer) ParseToken(token string, output interface{}) error {
	data, err := base64.URLEncoding.DecodeString(token)

	if err != nil {
		return err
	}

	hmac := data[len(data)-hmac_size:]
	payload := data[:len(data)-hmac_size]

	sha := sha256.Sum256(payload)

	sign, err := rsa.SignPKCS1v15(rand.Reader, r.privateKey, crypto.SHA256, sha[:])

	if err != nil {
		return err
	}

	if !bytes.Equal(hmac, sign[0:hmac_size]) {
		return errors.New("Token signature verification failed")
	}

	decrypt, err := rsa.DecryptPKCS1v15(rand.Reader, r.privateKey, payload)

	if err != nil {
		return err
	}

	return json.Unmarshal(decrypt, output)
}
