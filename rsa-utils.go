package secure_token

import (
	"crypto/rsa"
	"io/ioutil"
	"encoding/pem"
	"crypto/x509"
	"errors"
)

func getRsaPublicKeyFromBytes(publicKey []byte) (*rsa.PublicKey, error) {
	p, _ := pem.Decode(publicKey)

	if p == nil {
		return nil, errors.New("Invalid PublicKey")
	}

	pub, err := x509.ParsePKIXPublicKey(p.Bytes)

	if err != nil {
		return nil, err
	}

	return pub.(*rsa.PublicKey), nil
}

func getRsaPublicKeyFromPath(path string) (*rsa.PublicKey, error) {
	f, err := ioutil.ReadFile(path)

	if err != nil {
		return nil, err
	}

	return getRsaPublicKeyFromBytes(f)
}

func getRsaPublicKeyFromString(publicKey string) (*rsa.PublicKey, error) {
	return getRsaPublicKeyFromBytes([]byte(publicKey))
}

func getRsaPrivateKeyFromBytes(privateKey []byte) (*rsa.PrivateKey, error) {
	p, _ := pem.Decode(privateKey)

	if p == nil {
		return nil, errors.New("Invalid PrivateKey")
	}

	pub, err := x509.ParsePKCS1PrivateKey(p.Bytes)

	if err != nil {
		return nil, err
	}

	return pub, nil
}

func getRsaPrivateKeyFromPath(path string) (*rsa.PrivateKey, error) {
	f, err := ioutil.ReadFile(path)

	if err != nil {
		return nil, err
	}

	return getRsaPrivateKeyFromBytes(f)
}

func getRsaPrivateKeyFromString(privateKey string) (*rsa.PrivateKey, error) {
	return getRsaPrivateKeyFromBytes([]byte(privateKey))
}